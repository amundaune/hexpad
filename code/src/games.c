#include "games.h"
#include "keyboard.h"
#include "games.h"

bool play_simon(uint8_t scan);

bool gametime(void)
{
	static struct {
		uint8_t enabled   : 1;
		uint8_t simon    : 1;
		uint8_t moles    : 1;
		uint8_t reserved : 5;
	} play = {0x00};

	if((!play.enabled) && (keys[3].state & (1 << 5))
	        && ((keys[0].state & keys[0].change) & (1 << 0))) {
		udi_hid_kbd_up(HID_KEYPAD_NUM_LOCK);
//      if(!status.numlock) { //Did not work?
//          udi_hid_kbd_down(HID_KEYPAD_NUM_LOCK);
//          udi_hid_kbd_up(HID_KEYPAD_NUM_LOCK);
//      }
		all_keyleds_on(0, 40, 00);
		play.enabled = true;
	}

	if(play.enabled) {
		if(play.simon) {
			play.simon = play_simon(status.yscan);
		} else if(play.moles) {
//			play.moles = play_moles(status.yscan);
		} else if((status.yscan == 3)
		          && (keys[status.yscan].change & keys[status.yscan].state)) {
			switch(keys[status.yscan].state) {
			case(1<<0):
				all_keyleds_off();
				play.enabled = false;
				break;
			case(1<<1):
				play.simon = play_simon(status.yscan);
				break;
			case(1<<2):
//				play.moles = play_moles(status.yscan);
				break;
			default:
				break;
			}

			keys[status.yscan].change = 0x00;
		}
	}

	return play.enabled;
}


bool play_simon(uint8_t scan)
{
	//Need to put the bools into a byte.. GPIOR?
	bool play = true;
	static bool randomize = true, display = false, next_round = false;
	static uint8_t target = 2;
	static uint8_t count = 0;
	static uint16_t countdown = 0;
	static union {
		struct  {
			uint8_t y        : 2;
			uint8_t x        : 3;
			uint8_t on       : 1;
			uint8_t unused   : 2;
		};
		uint8_t byte;
	} game[32];

	if(randomize) {
		nvm_read(INT_EEPROM, EEPROM_SIMON_ADRESS, (void *)game, sizeof(uint32_t));
		uint32_t *seed = (uint32_t *)game;
		if((((game[0].byte == game[1].byte) == game[2].byte) == game[3].byte)) {
			//Do I need to init nvm stuff here? and reinit eeprom stuff?
			nvm_read_device_serial((struct nvm_device_serial *)game);
		}
		srandom(seed[0]);
		seed[0] = random();

		uint8_t i = 0;
		if(target > 4) {
			for(i = 0; i < (target / 4); i++) {
				seed[i + 1] = random();
			}
		}
		nvm_write(INT_EEPROM, EEPROM_SIMON_ADRESS, (void *)&seed[i], sizeof(uint32_t));

		all_keyleds_on(0, 80, 0);
		randomize = false;
		if(target == 2) {
			countdown = 1500;
		} else {
			countdown = 500;
		}
		game[0].on = 0;
		display = true;
		next_round = true;
	} else if(next_round) {
		if(0 == --countdown) {
			all_keyleds_off();
			next_round = false;
			countdown = 1000;
		}
	} else if(display) {
		if(0 == --countdown) {
			if(count != target) {
				if(game[count].on) {
					all_keyleds_off();
					countdown = 800;
					game[++count].on = 0;
				} else {
					//Can this be done in some more elegant way? bitfu?
					if(game[count].x >= XSIZE) {
						if(game[count].x == XSIZE) {
							game[count].x = XSIZE - 2;
						} else {
							game[count].x = 1;
						}
					}
					single_key_leds_on(game[count].x, game[count].y);
					countdown = 800;
					game[count].on = 1;
				}
			} else {
				count = 0;
				display = false;
			}
		}
	} else if(keys[scan].change & keys[scan].state) {
		keycount.l++;
		if((scan == game[count].y) && (keys[scan].state == (1 << game[count].x))) {
			if(++count == target) {
				target++;
				count = 0;
				randomize = true;
			}
		} else {
			all_keyleds_on(80, 0, 0);
			play = false;
			target = 2;
			count = 0;
			randomize = true;
		}
	}
	//add else if timeout (countdown) here?

	keys[scan].change = 0x00;
	return play;
}

