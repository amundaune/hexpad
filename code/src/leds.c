#include "asf.h"
#include "leds.h"
#include <sLED_driver.h>
#include <sLED_functions.h>

//Defines
#define ALL_LEDS      30
#define NUM_LOCK_LED        0
#define CAPS_LOCK_LED       1
#define SIZE_OF_LOCKLEDS  2

#define FIRST_ARRAY_LED     SIZE_OF_LOCKLEDS
#define LED_ARRAY_ROWS      7
#define LED_ARRAY_COLUMNS   4
#define SIZE_OF_ARRAYLEDS   (ALL_LEDS-SIZE_OF_LOCKLEDS)

//The LED memory
single_led_t leds[ALL_LEDS];

//Control variables for the color show
bool lock_configure = true, cafefun = false;
uint8_t colour_level = 0xc6; //Not a define because it can be changed?
single_led_t control_led = {0};

//Internal prototypes for the color show
uint8_t loop_ledarray_diagonal(uint8_t dir);

void leds_init(void)
{
	single_led_t led_config = {.red = 0x00, .green = 0x60, .blue = 0x00 };

#if defined(NDEBUG)
	delay_ms(1000); //Needed for LED startup
#endif
	sLED_driver_init();

	//add funky stuff that inits the leds in a different colour each time?
	sLED_array_update_all(leds, ALL_LEDS, SLED_MASK_RGB, led_config);
	sLED_driver_configure_leds(leds, ALL_LEDS);

	//Zero out the lock leds in memory to avoid wrong colors in first leds_update
	leds[NUM_LOCK_LED] = (const single_led_t) {
		0
	};
	leds[CAPS_LOCK_LED] = (const single_led_t) {
		0
	};
}

void leds_update(uint8_t value)
{
	single_led_t led_config = { 0 };

	if(value & HID_LED_NUM_LOCK) {
		leds[NUM_LOCK_LED].blue = 0x40;
	} else {
		leds[NUM_LOCK_LED] = (const single_led_t) {
			0
		};
		led_config.red = 0x40;
		led_config.green = 0x8;
	}

	if(value & HID_LED_CAPS_LOCK) {
		leds[CAPS_LOCK_LED].red = 0x60;
	} else {
		leds[CAPS_LOCK_LED] = (const single_led_t) {
			0
		};
	}

	//No need to do this while the cafefun is ongoing
	if(lock_configure) {
		sLED_array_update_all(&leds[FIRST_ARRAY_LED], SIZE_OF_ARRAYLEDS, SLED_MASK_RGB,
		                      led_config);
		sLED_driver_configure_leds(leds, ALL_LEDS);
	}
}

void leds_all_keys_off(void)
{
	single_led_t led_config = { 0 };

	sLED_array_update_all(&leds[FIRST_ARRAY_LED], SIZE_OF_ARRAYLEDS, SLED_MASK_RGB,
	                      led_config);
	sLED_driver_configure_leds(leds, ALL_LEDS);
}

void leds_all_keys_on(uint8_t red, uint8_t green, uint8_t blue)
{
	single_led_t led_config = { .red = red, .green = green, .blue = blue };

	sLED_array_update_all(&leds[FIRST_ARRAY_LED], SIZE_OF_ARRAYLEDS, SLED_MASK_RGB,
	                      led_config);
	sLED_driver_configure_leds(leds, ALL_LEDS);
}

void leds_single_key_on_random(uint8_t x, uint8_t y)
{
	leds_update_control_color();
	uint8_t target = FIRST_ARRAY_LED + (LED_ARRAY_ROWS-2 - x) + ((LED_ARRAY_COLUMNS - 1 - y) * LED_ARRAY_ROWS);
	leds[target++] = control_led;
	leds[target] = control_led;

	sLED_driver_configure_leds(leds, SIZE_OF_LOCKLEDS);
	sLED_driver_configure_leds_2darray_oddflip(&leds[FIRST_ARRAY_LED],
	        LED_ARRAY_ROWS, LED_ARRAY_COLUMNS);
}

void leds_key_row_on_random(uint8_t x, bool newcolour, bool configure)
{
	if (newcolour)
	{
		leds_update_control_color();
	}
	sLED_array_update_pixel_row(&leds[FIRST_ARRAY_LED], LED_ARRAY_ROWS,
	                            LED_ARRAY_COLUMNS, LED_ARRAY_ROWS-1 - x, SLED_MASK_RGB, control_led);
	sLED_array_update_pixel_row(&leds[FIRST_ARRAY_LED], LED_ARRAY_ROWS,
	                            LED_ARRAY_COLUMNS, LED_ARRAY_ROWS-2 - x, SLED_MASK_RGB, control_led);

	if (configure) {
		sLED_driver_configure_leds(leds, SIZE_OF_LOCKLEDS);
		sLED_driver_configure_leds_2darray_oddflip(&leds[FIRST_ARRAY_LED],
				LED_ARRAY_ROWS, LED_ARRAY_COLUMNS);
	}
}

void leds_key_column_on_random(uint8_t y, bool newcolour, bool configure)
{
	if (newcolour)
	{
		leds_update_control_color();
	}

	sLED_array_update_pixel_column(&leds[FIRST_ARRAY_LED], LED_ARRAY_ROWS,
	                               LED_ARRAY_COLUMNS, LED_ARRAY_COLUMNS - 1 - y, SLED_MASK_RGB, control_led);

	if (configure) {
		sLED_driver_configure_leds(leds, SIZE_OF_LOCKLEDS);
		sLED_driver_configure_leds_2darray_oddflip(&leds[FIRST_ARRAY_LED],
		LED_ARRAY_ROWS, LED_ARRAY_COLUMNS);
	}
}

void leds_configure_lock_update(bool state)
{
	lock_configure = state;
}

void start_fun_with_cafeleds(void)
{
	leds_configure_lock_update(STOP);
	cafefun = true;
}

void stop_fun_with_cafeleds(void)
{
	cafefun = false;
	leds_configure_lock_update(START);
}

void fun_with_cafeleds(void)
{
	static uint8_t state = 0;

	if(cafefun) {
		state += loop_ledarray_diagonal(state);
		if(3 < state) {
			state = 0;
		}

		sLED_driver_configure_leds(leds, SIZE_OF_LOCKLEDS);
		sLED_driver_configure_leds_2darray_oddflip(&leds[FIRST_ARRAY_LED],
		        LED_ARRAY_ROWS, LED_ARRAY_COLUMNS);
	}
}

void leds_update_control_color(void)
{
	if(control_led.red == colour_level / 2) {
		if(control_led.green > 0) {                    //red+blue = purple
			control_led.green = 0;
			control_led.blue = colour_level / 2;
		} else {                                       //blue+green = cyan
			control_led.red = 0;
			control_led.green = colour_level / 2;
		}
	} else if(control_led.green == colour_level / 2) { //all = white
		control_led.blue = colour_level / 3;
		control_led.green = colour_level / 3;
		control_led.red = colour_level / 3;
	} else if(control_led.blue == colour_level / 3) {  //red
		control_led.blue = 0;
		control_led.green = 0;
		control_led.red = colour_level;
	} else if(control_led.red > 0) {                   //green
		control_led.red = 0;
		control_led.green = colour_level;
	} else if(control_led.green > 0) {                 //blue
		control_led.green = 0;
		control_led.blue = colour_level;
	} else {                                           //red+green = yellow
		control_led.blue = 0;
		control_led.red = colour_level / 2;
		control_led.green = colour_level / 2;
	}
}


uint8_t loop_ledarray_diagonal(uint8_t dir)
{
	static uint8_t count = 0;
	static int8_t updatedir = 1;
	uint8_t finished = 0;

	if(0 == dir) {
		sLED_array_update_pixel_diagonal_rising(&leds[FIRST_ARRAY_LED],
		                                        LED_ARRAY_ROWS,
		                                        LED_ARRAY_COLUMNS,
		                                        count,
		                                        SLED_MASK_RGB,
		                                        control_led);
	} else if(1 == dir) {
		sLED_array_update_pixel_diagonal_falling(&leds[FIRST_ARRAY_LED],
		        LED_ARRAY_ROWS,
		        LED_ARRAY_COLUMNS,
		        count,
		        SLED_MASK_RGB,
		        control_led);
	} else if(2 == dir) {
		sLED_array_update_pixel_diagonal_rising(&leds[FIRST_ARRAY_LED],
		                                        LED_ARRAY_ROWS,
		                                        LED_ARRAY_COLUMNS,
		                                        LED_ARRAY_ROWS + LED_ARRAY_COLUMNS - 2 - count,
		                                        SLED_MASK_RGB,
		                                        control_led);
	} else {
		sLED_array_update_pixel_diagonal_falling(&leds[FIRST_ARRAY_LED],
		        LED_ARRAY_ROWS,
		        LED_ARRAY_COLUMNS,
		        LED_ARRAY_ROWS + LED_ARRAY_COLUMNS - 2 - count,
		        SLED_MASK_RGB,
		        control_led);
	}

	count += updatedir;
	if(LED_ARRAY_ROWS + LED_ARRAY_COLUMNS - 2 < count) {
		finished = 1;
		count  = 0;
		leds_update_control_color();
	}

	return finished;
}