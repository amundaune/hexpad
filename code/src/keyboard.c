/*
 * keyboard.c
 *
 * Created: 08.12.2016 11:14:28
 * Author : M43977
 */
#include <avr/interrupt.h>
#include <avr/cpufunc.h>
#include "asf.h"
#include "keyboard.h"

#define XSIZE 6
#define YSIZE 4
#define XMASK ((1<<XSIZE)-1)
#define YMASK ((1<<YSIZE)-1)

#define EEPROM_KEYCOUNT_ADRESS   EEPROM_START
#define EEPROM_ELITE_ADRESS      (EEPROM_KEYCOUNT_ADRESS + sizeof(uint32_t))

//Set to GP registers mostly because I initially wanted to use interrupts
volatile union {
	struct  {
		uint8_t numlock         : 1;
		uint8_t capslock        : 1;
		uint8_t reserved        : 4;
		uint8_t yscan           : 2;
	};
	uint8_t reg;
} status __attribute__((io(&GPIOR0)));

volatile struct  {
	uint8_t state;
	uint8_t change;
} keys[YSIZE] __attribute__((io(&GPIOR1)));

static const struct {
	uint8_t key;
	uint8_t modifier;
} hid_values[2][YSIZE][XSIZE] = {
	{	{   NOTLOCK_BUTTON00, NOTLOCK_BUTTON01, NOTLOCK_BUTTON02, NOTLOCK_BUTTON03, NOTLOCK_BUTTON04, NOTLOCK_BUTTON05  },
		{   NOTLOCK_BUTTON10, NOTLOCK_BUTTON11, NOTLOCK_BUTTON12, NOTLOCK_BUTTON13, NOTLOCK_BUTTON14, NOTLOCK_BUTTON15  },
		{   NOTLOCK_BUTTON20, NOTLOCK_BUTTON21, NOTLOCK_BUTTON22, NOTLOCK_BUTTON23, NOTLOCK_BUTTON24, NOTLOCK_BUTTON25  },
		{   NOTLOCK_BUTTON30, NOTLOCK_BUTTON31, NOTLOCK_BUTTON32, NOTLOCK_BUTTON33, NOTLOCK_BUTTON34, NOTLOCK_BUTTON35  }
	},
	{	{   NUMLOCK_BUTTON00, NUMLOCK_BUTTON01, NUMLOCK_BUTTON02, NUMLOCK_BUTTON03, NUMLOCK_BUTTON04, NUMLOCK_BUTTON05  },
		{   NUMLOCK_BUTTON10, NUMLOCK_BUTTON11, NUMLOCK_BUTTON12, NUMLOCK_BUTTON13, NUMLOCK_BUTTON14, NUMLOCK_BUTTON15  },
		{   NUMLOCK_BUTTON20, NUMLOCK_BUTTON21, NUMLOCK_BUTTON22, NUMLOCK_BUTTON23, NUMLOCK_BUTTON24, NUMLOCK_BUTTON25  },
		{   NUMLOCK_BUTTON30, NUMLOCK_BUTTON31, NUMLOCK_BUTTON32, NUMLOCK_BUTTON33, NUMLOCK_BUTTON34, NUMLOCK_BUTTON35  }
	}
};

static volatile uint32_t keycount = 0, elite = 0;

//Internal prototypes
bool keychange_add(uint8_t y, uint8_t x, uint8_t down, uint8_t numlock);
void cafebabe(uint8_t key);
bool play_simon(uint8_t scan);
bool play_moles(uint8_t scan);
bool play_rockstar(uint8_t scan);

void keyscan_init(void)
{
	//Read out stored keycount
	if((nvm_read(INT_EEPROM, EEPROM_KEYCOUNT_ADRESS, (void *)&keycount,
	             sizeof(keycount)) != STATUS_OK)
	        || (nvm_read(INT_EEPROM, EEPROM_ELITE_ADRESS, (void *)&elite,
	                     sizeof(elite)) != STATUS_OK)) {
		//Check read content
		while(true) {
			//TODO: Something? or just ignore?
		}
	}

	//In case eeprom has been cleared or something?
	if(-1 == keycount) {
		keycount = 0;
	}
	if((1337 > elite) || (-1 == elite)) {
		elite = 1337;
		nvm_write(INT_EEPROM, EEPROM_ELITE_ADRESS, (void *)&elite, sizeof(elite));
	}

	//OUTPUT High on all during init
	PORTB.OUTSET = YMASK;
	PORTB.DIRSET = YMASK;
	PORTCFG.MPCMASK = YMASK;
	PORTB.PIN0CTRL = PORT_SRLEN_bm;   //Slew rate on output?

	//INPUT w/Pullup on rows, inverted pins
	PORTA.DIRCLR = XMASK;
	PORTA.OUTCLR = XMASK;
	PORTCFG.MPCMASK = XMASK;
	PORTA.PIN0CTRL = PORT_OPC_PULLUP_gc | PORT_INVEN_bm;

	//For some reason this fixed a startup issue
	keychange_scan();
	keychange_scan();
	keychange_scan();
	keychange_scan();

	//Add boot keycheck here? When pressing 0 and F during startup do something?
	if((keys[1].state & (1 << 0)) && (keys[3].state & (1 << 5))) {
		start_fun_with_cafeleds();
	}
}

void keychange_process(void)
{
	//Below comments are irrelevant as long as processing is done in the sof handler
	//Store which y-line we are processing, in case next scan starts before we are done.
	//How would it work to just put status.yscan in the function call, as not volatile?
	uint8_t y = status.yscan;

	if(keys[y].change) {
		for(uint8_t x = 0; x < XSIZE; x++) {
			if(keys[y].change & (1 << x)) {
				if(keychange_add(y, x, keys[y].state & (1 << x), status.numlock)) {
					keys[y].change &= ~(1 << x);
					keycount++;
				}
			}
		}
	}
}

bool keychange_add(uint8_t y, uint8_t x, uint8_t down, uint8_t numlock)
{
	//Get key-combination from array and send first modifiers and then the normal key value, opposite if releasing key
	bool success = true;
	if(down) {
		if(hid_values[numlock][y][x].modifier & HID_MODIFIER_LEFT_CTRL) {
			success &= udi_hid_kbd_modifier_down(HID_MODIFIER_LEFT_CTRL);
		}
		if(hid_values[numlock][y][x].modifier & HID_MODIFIER_LEFT_SHIFT) {
			success &= udi_hid_kbd_modifier_down(HID_MODIFIER_LEFT_SHIFT);
		}
		if(hid_values[numlock][y][x].modifier & HID_MODIFIER_LEFT_ALT) {
			success &= udi_hid_kbd_modifier_down(HID_MODIFIER_LEFT_ALT);
		}
		if(hid_values[numlock][y][x].key) {
			success &= udi_hid_kbd_down(hid_values[numlock][y][x].key);
		}
		cafebabe(hid_values[numlock][y][x].key);
	} else {
		if(hid_values[numlock][y][x].key) {
			success &= udi_hid_kbd_up(hid_values[numlock][y][x].key);
		}
		if(hid_values[numlock][y][x].modifier & HID_MODIFIER_LEFT_ALT) {
			success &= udi_hid_kbd_modifier_up(HID_MODIFIER_LEFT_ALT);
		}
		if(hid_values[numlock][y][x].modifier & HID_MODIFIER_LEFT_SHIFT) {
			success &= udi_hid_kbd_modifier_up(HID_MODIFIER_LEFT_SHIFT);
		}
		if(hid_values[numlock][y][x].modifier & HID_MODIFIER_LEFT_CTRL) {
			success &= udi_hid_kbd_modifier_up(HID_MODIFIER_LEFT_CTRL);
		}
	}

	return success;
}

void keystate_update(uint8_t value)
{
	if(value & HID_LED_NUM_LOCK) {
		status.numlock = 1;
	} else {
		status.numlock = 0;
	}

	if(value & HID_LED_CAPS_LOCK) {
		status.capslock = 1;
	} else {
		status.capslock = 0;
	}
}

//ISR(TCE0_OVF_vect)
void keychange_scan(void)
{
	//Scan next y-line by pulling it low
	//Pre-increment to make y usable by processing. Bitfield will loop back to 0 when incrementing 3 right?
	PORTB.OUTCLR = (1 << ++status.yscan);

	//Do I need any delay here? Due to slew rate and external components?
	_NOP();
	_NOP();
	_NOP();
	_NOP();
	_NOP();
	_NOP();
	_NOP();
	_NOP();

	//Update x-lines by updating new states and finding changes
	keys[status.yscan].change = keys[status.yscan].state ^ (PORTA.IN & XMASK);
	keys[status.yscan].state = (PORTA.IN & XMASK);

	//Pull y-line high again
	PORTB.OUTSET = YMASK;
}

uint8_t keycount_store(void)
{
	static uint32_t prev_keycount = 0; //TODO: Don't need volatile here right?
	uint8_t status = STATUS_OK;

	if(prev_keycount != keycount) {
		if(prev_keycount == 0) {
			prev_keycount = keycount;
		} else if((status = nvm_write(INT_EEPROM, EEPROM_KEYCOUNT_ADRESS,
		                              (void *)&keycount, sizeof(keycount))) == STATUS_OK) {
			prev_keycount = keycount;
			if(keycount >= elite) {
				elite <<= 2;    //Will never overflow with uint32t..
				status = nvm_write(INT_EEPROM, EEPROM_ELITE_ADRESS, (void *)&elite,
				                   sizeof(elite));
				leds_all_keys_on(13, 33, 37); //Need to improve!
			}
		}
	}

	return status;
}


void cafebabe(uint8_t key)
{
	static uint8_t babe = 0;
	const uint8_t cafe[8] = {6, 4, 9, 8, 5, 4, 5, 8};

	if(key != cafe[babe++]) {
		babe = 0;
	}

	if(babe > 7) {
		start_fun_with_cafeleds();
		babe = 0;
	} else {
		stop_fun_with_cafeleds();
	}
}


bool gametime(void)
{
	static struct {
		uint8_t enabled  : 1;
		uint8_t simon    : 1;
		uint8_t moles    : 1;
		uint8_t rockstar : 1;
		uint8_t reserved : 4;
	} play = {0};

	if((!play.enabled) && (keys[3].state & (1 << 5))
	        && ((keys[0].state & keys[0].change) & (1 << 0))) {
		udi_hid_kbd_up(HID_KEYPAD_NUM_LOCK);
		leds_configure_lock_update(STOP);
//      if(!status.numlock) { //Did not work? Need delay between?
//          udi_hid_kbd_down(HID_KEYPAD_NUM_LOCK);
//          udi_hid_kbd_up(HID_KEYPAD_NUM_LOCK);
//      }
		leds_all_keys_on(0, 40, 00);
		play.enabled = true;
	}

	if(play.enabled) {
		if(play.simon) {
			play.simon = play_simon(status.yscan);
		} else if(play.moles) {
			play.moles = play_moles(status.yscan);
		} else if(play.rockstar) {
			play.rockstar = play_rockstar(status.yscan);
		} else if((status.yscan == 3)
		          && (keys[status.yscan].change & keys[status.yscan].state)) {
			switch(keys[status.yscan].state) {
			case(1<<0):
				leds_configure_lock_update(START);
				leds_all_keys_off();
				play.enabled = false;
				break;
			case(1<<1):
				play.simon = true;
				break;
			case(1<<2):
				play.moles = true;
				break;
			case(1<<3):
				play.rockstar = true;
				break;
			default:
				break;
			}

			keys[status.yscan].change = 0x00;
		}
	}

	return play.enabled;
}

bool play_simon(uint8_t scan)
{
	//Need to put the bools into a byte.. GPIOR?
	bool play = true;
	static bool init = true, randomize = true, display = false, next_round = false;
	static uint8_t target = 2;
	static uint8_t count = 0;
	static uint16_t countdown = 0;
	static union {
		struct  {
			uint8_t y        : 2;
			uint8_t x        : 3;
			uint8_t on       : 1;
			uint8_t unused   : 2;
		};
		uint8_t byte;
	} game[32];

	if(init) {
		uint32_t *seed = (uint32_t *)game;
		srandom(keycount);
		seed[0] = random();
		seed[0] = random();
		init = false;
	}

	if(randomize) {
		uint32_t *seed = (uint32_t *)game;
		seed[0] = random();

		uint8_t i = 0;
		if(target > 4) {
			for(i = 0; i < (target / 4); i++) {
				seed[i + 1] = random();
			}
		}

		leds_all_keys_on(0, 80, 0);
		randomize = false;
		if(target == 2) {
			countdown = 1500;
		} else {
			countdown = 500;
		}
		game[0].on = 0;
		display = true;
		next_round = true;
	} else if(next_round) {
		if(0 == --countdown) {
			leds_all_keys_off();
			next_round = false;
			countdown = 1000;
		}
	} else if(display) {
		if(0 == --countdown) {
			if(count != target) {
				if(game[count].on) {
					leds_all_keys_off();
					countdown = 800;
					game[++count].on = 0;
				} else {
					//Can this be done in some more elegant way? bitfu?
					if(game[count].x >= XSIZE) {
						if(game[count].x == XSIZE) {
							game[count].x = XSIZE - 2;
						} else {
							game[count].x = 1;
						}
					}
					leds_single_key_on_random(game[count].x, game[count].y);
					countdown = 800;
					game[count].on = 1;
				}
			} else {
				count = 0;
				display = false;
			}
		}
	} else if(keys[scan].change & keys[scan].state) {
		keycount++;
		if((scan == game[count].y) && (keys[scan].state == (1 << game[count].x))) {
			if(++count == target) {
				target++;
				count = 0;
				randomize = true;
			}
		} else {
			leds_all_keys_on(80, 0, 0);
			play = false;
			target = 2;
			count = 0;
			randomize = true;
		}
	}
	//add else if timeout (countdown) here?

	keys[scan].change = 0x00;
	return play;

}

bool play_moles(uint8_t scan)
{
	uint8_t play = true;

	play = false;

	return play;
}

#define ROCKSTAR_PERIOD       720
#define ROCKSTAR_OFF_TIME	  0
#define ROCKSTAR_ON_TIME	  (ROCKSTAR_PERIOD/2)
#define ROCKSTAR_RESET_TIME   (-(ROCKSTAR_ON_TIME/2))
#define ROCKSTAR_PERIOD_TIME  ((ROCKSTAR_PERIOD*3)/4)

bool play_rockstar(uint8_t scan)
{
	bool play = true, init = true;
	static uint8_t targeta = 1, targetb = 2, target_switch = 2;
	static int16_t countdown = ROCKSTAR_PERIOD*2;
	static uint32_t seed = 0;
	static uint8_t seed_shift = 0;

	if(init) {
		seed = keycount;
		srandom(seed);
		seed = random();
		seed = random();
		init = false;
	}

	if(9 == seed_shift) {
		seed = random();
		seed_shift = 0;
	}

	switch(countdown) {
	case(ROCKSTAR_OFF_TIME):
		leds_all_keys_off();
		break;
	case(ROCKSTAR_ON_TIME):
		leds_key_row_on_random(targeta, false, (target_switch != 1));
		if((targeta != targetb) && (target_switch == 1)) {
			leds_key_row_on_random(targetb, true, true);
		}
		break;
	case(ROCKSTAR_RESET_TIME):
		countdown = ROCKSTAR_PERIOD_TIME;
	case(ROCKSTAR_PERIOD+ROCKSTAR_ON_TIME):
		if(targeta == targetb) {
			targetb = (seed >> (3 * seed_shift++));
			if((targetb & 0x07) < XSIZE) {
				targetb &= 0x7;
			} else if(((targetb >> 1) & 0x07) < XSIZE) {
				targetb = (targetb >> 1) & 0x07;
			} else {
				targetb = (targetb & 0x07) - 4;
			}

			target_switch = ((seed & 0x07) << 1) + (9 - seed_shift); //Stupid?
		} else {
			if(0 == --target_switch) {
				targeta = targetb;
			}
		}
		break;
	case(ROCKSTAR_PERIOD*2):
		leds_all_keys_off();
		break;
	default:
		break;
	}
	countdown--;

	//Have some kind of countdown on the first "beats" to avoid scoring..

	if(keys[scan].change & keys[scan].state) {
		keycount++;
		if(keys[scan].state & (1 << targeta)) {
			//Somethingsomething abs(countdown)/32 at least
		} else {
			leds_all_keys_on(80, 0, 0);
			//Figure out score here?
			play = false;
			//Reinit stuff here..
			countdown = 3000;
		}
		keys[scan].change = 0x00;
	}

	return play;
}