
#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

//Normal hexpad config when numlock is on, remove everything debug? TODO: Shift or not on A-F??
#define NUMLOCK_BUTTON05    {HID_D                 , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON04    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON03    {HID_KEYPAD_7          , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON02    {HID_KEYPAD_4          , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON01    {HID_KEYPAD_1          , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON00    {HID_KEYPAD_0          , HID_MODIFIER_NONE                                  }

#define NUMLOCK_BUTTON15    {HID_E                 , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON14    {HID_B                 , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON13    {HID_KEYPAD_8          , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON12    {HID_KEYPAD_5          , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON11    {HID_KEYPAD_2          , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON10    {HID_X                 , HID_MODIFIER_NONE                                  }

#define NUMLOCK_BUTTON25    {HID_F                 , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON24    {HID_C                 , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON23    {HID_KEYPAD_9          , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON22    {HID_KEYPAD_6          , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON21    {HID_KEYPAD_3          , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON20    {HID_COMMA             , HID_MODIFIER_NONE                                  }

#define NUMLOCK_BUTTON35    {HID_KEYPAD_NUM_LOCK   , HID_MODIFIER_NONE                                  }
#define NUMLOCK_BUTTON34    {HID_R                 , (HID_MODIFIER_LEFT_SHIFT | HID_MODIFIER_LEFT_ALT)  }   //Replace/Move to notlock..
#define NUMLOCK_BUTTON33    {HID_F                 , (HID_MODIFIER_LEFT_SHIFT | HID_MODIFIER_LEFT_ALT)  }   //Replace/Move to notlock..
#define NUMLOCK_BUTTON32    {HID_G                 , HID_MODIFIER_LEFT_ALT                              }   //Replace/Move to notlock..
#define NUMLOCK_BUTTON31    {HID_F5                , (HID_MODIFIER_LEFT_CTRL | HID_MODIFIER_LEFT_ALT)   }   //Replace/Move to notlock..
#define NUMLOCK_BUTTON30    {HID_KEYPAD_ENTER      , HID_MODIFIER_NONE                                  }


//What to do when numlock is off? Complete debug-fu??
#define NOTLOCK_BUTTON05    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON04    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON03    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON02    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON01    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON00    {HID_A                 , HID_MODIFIER_NONE                                  }

#define NOTLOCK_BUTTON15    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON14    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON13    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON12    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON11    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON10    {HID_F7                , (HID_MODIFIER_LEFT_CTRL | HID_MODIFIER_LEFT_ALT)   }

#define NOTLOCK_BUTTON25    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON24    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON23    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON22    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON21    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON20    {HID_A                 , HID_MODIFIER_NONE                                  }

#define NOTLOCK_BUTTON35    {HID_KEYPAD_NUM_LOCK   , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON34    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON33    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON32    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON31    {HID_A                 , HID_MODIFIER_NONE                                  }
#define NOTLOCK_BUTTON30    {HID_KEYPAD_ENTER      , HID_MODIFIER_NONE                                  }

/**
 * \brief
 *
 * \param
 *
 * \return void
 */
void keyscan_init(void);

/**
 * \brief
 *
 * \param
 *
 * \return void
 */
void keychange_process(void);

/**
 * \brief
 *
 * \param value
 *
 * \return void
 */
void keystate_update(uint8_t value);

/**
 * \brief
 *
 * \param
 *
 * \return void
 */
void keychange_scan(void);


uint8_t keycount_store(void);
bool gametime(void);

#endif /* _KEYBOARD_H_ */
