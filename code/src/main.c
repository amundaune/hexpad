/**
 * \file
 *
 * \brief Main functions for Keyboard example
 *
 * Copyright (c) 2009-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include <asf.h>
#include "conf_usb.h"
//#include "ui.h"
#include "keyboard.h"
#include "leds.h"

static volatile bool main_b_kbd_enable = false;

/*! \brief Main function. Execution starts here.
 */
int main(void)
{
	//Example enables interrupt as the first thing?
	irq_initialize_vectors();
	cpu_irq_enable();

	// Initialize asf stuff
	sleepmgr_init();
	sysclk_init();
	board_init();

	//Set PORT registers to power reduction?
	//TBD

#if !defined(NDEBUG)    //Debug stuff
	//STK600 Leds
	LED_On(LED0_GPIO);
	LED_Off(LED1_GPIO);
	LED_Off(LED0_GPIO);

	//For data visualizer
	PORTC.DIR = 0xff;
	PORTC.OUT = 0x00;
#endif // NDEBUG

	//Init LEDs and keys
	leds_init();
	keyscan_init();

	// Start USB stack to authorize VBus monitoring
	udc_start();

	// The main loop manages only the power mode
	// because the USB management is done by interrupt
	while(true) {
		sleepmgr_enter_sleep();
	}
}

void main_suspend_action(void)
{
#if !defined(NDEBUG)    //Debug stuff
	//STK600 Leds
	LED_Off(LED0_GPIO);
	LED_Off(LED1_GPIO);
#endif // NDEBUG
}

void main_resume_action(void)
{
#if !defined(NDEBUG)    //Debug stuff
	//STK600 Leds
	LED_On(LED0_GPIO);
#endif // NDEBUG
}

void main_sof_action(void)
{
	if(!main_b_kbd_enable) {
		return;
	}

#if !defined(NDEBUG)    //Debug stuff
	//STK600 Leds
	if((udd_get_frame_number() % 1000) == 0) {
		LED_On(LED1_GPIO);
	}
	if((udd_get_frame_number() % 1000) == 500) {
		LED_Off(LED1_GPIO);
	}

	PORTC.OUTSET = PIN1_bm | PIN0_bm; //Data visualizer

#endif // NDEBUG

	//Check the keys and process stuff!
	keychange_scan();
	if(!gametime()) {
		keychange_process();
	}

#if !defined(NDEBUG)    //Data visualizer
	PORTC.OUTCLR = PIN1_bm;
#endif // NDEBUG

	if((udd_get_frame_number() % 66) == 0) {
		fun_with_cafeleds();
	}

	//Only store keycount every 5 sec, no need to do it all the time..
	if((udd_get_frame_number() % 5000) == 0) {
		if(keycount_store() != STATUS_OK) {
			//TODO: Something..
		}
	}

#if !defined(NDEBUG)    //Data visualizer
	PORTC.OUTCLR = PIN0_bm;
#endif // NDEBUG

}

void main_remotewakeup_enable(void)
{
//	ui_wakeup_enable();
}

void main_remotewakeup_disable(void)
{
//	ui_wakeup_disable();
}

bool main_kbd_enable(void)
{
	main_b_kbd_enable = true;
	return true;
}

void main_kbd_disable(void)
{
	main_b_kbd_enable = false;
}
